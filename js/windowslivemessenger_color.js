if (Drupal.jsEnabled) {
  $(document).ready(function () {
    var form = $('#msnlive_color_scheme_form .color-form');

    var inputs = [];
    var focused = null;

	//show color info only with option "Button"
	//$('.blockstyle-form input').change(changeBlockstyle);

    var typeSelect = document.getElementById('edit-msnlive-color-type');
    var lastType = $(typeSelect).val();

    // Add Farbtastic
    $(form).prepend('<div id="placeholder"></div>');
    var farb = $.farbtastic('#placeholder');

    // Decode reference colors to HSL
    var reference = Drupal.settings.msnlive.reference;
    for (i in reference) {
      if (reference[i]) {
        reference[i] = farb.RGBToHSL(farb.unpack(reference[i]));
      }
    }

    // Setup color display select.
    //$(typeSelect).change(changeType);


	function changeBlockstyle(){
		var blockstyleOption = $('.blockstyle-form input:checked').val();
		if(blockstyleOption==1){
			$('#msnlive_color_scheme_form').show();
		}else{
			$('#msnlive_color_scheme_form').hide();		
		}	
	}

    /**
     * Show or hide the second set of colors.
     */
    function changeType() {
    
      if ($(typeSelect).val() == 'solid') {
        $('#msnlive-palette').show();
        $(inputs).attr('disabled', false);
        $('.lock', form).hide();
        $('#placeholder').show();
      }
      else {
        $(inputs).attr('disabled', true);
        $('#msnlive-palette').hide();
        $('#placeholder').hide();
      }
		
    }


    /**
     * Shift a given color, using a reference pair (ref in HSL).
     *
     * This algorithm ensures relative ordering on the saturation and luminance
     * axes is preserved, and performs a simple hue shift.
     *
     * It is also symmetrical. If: shift_color(c, a, b) == d,
     *                        then shift_color(d, b, a) == c.
     */
    function shift_color(given, ref1, ref2) {
      // Convert to HSL
      given = farb.RGBToHSL(farb.unpack(given));

      // Hue: apply delta
      given[0] += ref2[0] - ref1[0];

      // Saturation: interpolate
      if (ref1[1] == 0 || ref2[1] == 0) {
        given[1] = ref2[1];
      }
      else {
        var d = ref1[1] / ref2[1];
        if (d > 1) {
          given[1] /= d;
        }
        else {
          given[1] = 1 - (1 - given[1]) * d;
        }
      }

      // Luminance: interpolate
      if (ref1[2] == 0 || ref2[2] == 0) {
        given[2] = ref2[2];
      }
      else {
        var d = ref1[2] / ref2[2];
        if (d > 1) {
          given[2] /= d;
        }
        else {
          given[2] = 1 - (1 - given[2]) * d;
        }
      }

      return farb.pack(farb.HSLToRGB(given));
    }

    /**
     * Callback for Farbtastic when a new color is chosen.
     */
    function callback(input, color, propagate, colorscheme) {
      // Set background/foreground color
      $(input).css({
        backgroundColor: color,
        color: farb.RGBToHSL(farb.unpack(color))[2] > 0.5 ? '#000' : '#fff'
      });

      // Change input value
      if (input.value != color) {
        input.value = color;
      }

    }

    // Focus the Farbtastic on a particular field.
    function focus() {
      var input = this;

      // Remove the transparent text if any.
      if (this.key == 'matte' && input.value == Drupal.settings.msnlive.transparent) {
        this.value = '';
      }

      // Remove old bindings
      focused && $(focused).unbind('keyup', farb.updateValue)
          .parent().removeClass('item-selected');

      // Add new bindings
      focused = this;
      farb.linkTo(function (color) { callback(input, color, true, false) });
      farb.setColor(this.value);
      $(focused).keyup(farb.updateValue)
        .parent().addClass('item-selected');
    }

    function blur() {
      if (this.key == 'matte' && (!this.value || this.value == Drupal.settings.msnlive.transparent)) {
        this.value = Drupal.settings.msnlive.transparent;
        $(this).css({
          backgroundColor: '#fff',
          color: '#ccc'
        });
      }
      if (this.key != 'matte') {
        if (!this.value) {
          //var rgb = this.style.backgroundColor.replace(/rgb\(([0-9, ]+)\)/, '$1').split(", ");
          //rgb[0] = rgb[0] / 255;
          //rgb[1] = rgb[1] / 255;
          //rgb[2] = rgb[2] / 255;
          //this.value = farb.pack(rgb);
        }
      }
    }

    // Initialize color fields
    $('#msnlive-palette input.form-text', form)
    .each(function () {
      // Extract palette field name
      this.key = this.id.substring(21);

      // Link to color picker temporarily to initialize.
      farb.linkTo(function () {}).setColor('#000').linkTo(this);

      this.i = i;
      inputs.push(this);
    })
    .focus(focus)
    .blur(blur);

    // Blur the matte color.
    blur.call(inputs[6]);

    // Focus first color
    focus.call(inputs[0]);

    // Hide secondary color fields and preview (called in changeType).
    //changeType();
    
    //hide or show color based on Blocktype
	//changeBlockstyle();
  });
}
