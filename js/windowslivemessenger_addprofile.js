$(document).ready(function(){
	$("#msnlive_color_scheme_form").toggle();
	
	$("input[@name='block']").change(function () {
		if ($("input[@name='block']:checked").val() == "1") {
			$('#edit-window-height').parent().hide();
			
			if ($("input[@name='block_style']:checked").val() == 0) {
				//Fields to hide
				hide = new Array('linkColor', 'borderColor', 'buttonForeColor', 'buttonBackColor', 'buttonBorderColor', 'buttonDisabledColor', 'headerForeColor', 'headerBackColor', 'menuForeColor', 'menuBackColor', 'chatForeColor', 'chatBackColor', 'chatDisabledColor', 'chatErrorColor', 'chatLabelColor');
	
				for (i = 0; i < hide.length; i++) {
					$('#edit-window-' + hide[i]).parent().hide();
				}
				
				if (window.location.href.indexOf('editprofile') == -1) {
					color = '#D7E8EC';
					$('#edit-window-backColor').attr('value', color);
					$('#edit-window-backColor').css({
						backgroundColor: color,
					});
					
					color = '#424542';
					$('#edit-window-foreColor').attr('value', color);
					$('#edit-window-foreColor').css({
						backgroundColor: color,
					});
				}
			}
			
			if ($("#msnlive_color_scheme_form").is(":visible") == false) {
				$('#msnlive_color_scheme_form').toggle();
			}
		} else {
			$('#edit-window-height').parent().show();
			
			if ($("input[@name='block_style']:checked").val() == "0") {
				if ($("#msnlive_color_scheme_form").is(":visible") == true) {
					$('#msnlive_color_scheme_form').toggle();
				}
			}
		}
		
		//Default sizes
		switch ($("input[@name='block']:checked").val()) {
			case "0":
				width="300";
				height="300";
			break;
				
			case "1":
				width="100";
				height="0";
			break;
			
			case "2":
				width="16";
				height="16";
			break;
		}
		
		$('#edit-window-width').attr('value', width);
		$('#edit-window-height').attr('value', height);
	});
	
	$("input[@name='block_style']").change(function () {
		if ($("input[@name='block']:checked").val() != "1") {
			if ($("#msnlive_color_scheme_form").is(":visible") == false) {
				show = new Array('linkColor', 'borderColor', 'buttonForeColor', 'buttonBackColor', 'buttonBorderColor', 'buttonDisabledColor', 'headerForeColor', 'headerBackColor', 'menuForeColor', 'menuBackColor', 'chatForeColor', 'chatBackColor', 'chatDisabledColor', 'chatErrorColor', 'chatLabelColor');
				
				for (i = 0; i < show.length; i++) {
					$('#edit-window-' + show[i]).parent().show();
				}
				
				$("#msnlive_color_scheme_form").toggle();
			} else {
				$("#msnlive_color_scheme_form").toggle();
			}
		} else {
			if ($("input[@name='block_style']:checked").val() == "1") {
				show = new Array('linkColor', 'borderColor', 'buttonForeColor', 'buttonBackColor', 'buttonBorderColor', 'buttonDisabledColor', 'headerForeColor', 'headerBackColor', 'menuForeColor', 'menuBackColor', 'chatForeColor', 'chatBackColor', 'chatDisabledColor', 'chatErrorColor', 'chatLabelColor');
				
				for (i = 0; i < hide.length; i++) {
					$('#edit-window-' + hide[i]).parent().show();
				}
			} else {
				hide = new Array('linkColor', 'borderColor', 'buttonForeColor', 'buttonBackColor', 'buttonBorderColor', 'buttonDisabledColor', 'headerForeColor', 'headerBackColor', 'menuForeColor', 'menuBackColor', 'chatForeColor', 'chatBackColor', 'chatDisabledColor', 'chatErrorColor', 'chatLabelColor');
				
				for (i = 0; i < hide.length; i++) {
					$('#edit-window-' + hide[i]).parent().hide();
				}
			}
		}
	});
	
	if ($("input[@name='block']:checked").val() == 1) {
		$('#edit-window-height').parent().hide();
		
		if ($("input[@name='block_style']:checked").val() == "0") {
			hide = new Array('linkColor', 'borderColor', 'buttonForeColor', 'buttonBackColor', 'buttonBorderColor', 'buttonDisabledColor', 'headerForeColor', 'headerBackColor', 'menuForeColor', 'menuBackColor', 'chatForeColor', 'chatBackColor', 'chatDisabledColor', 'chatErrorColor', 'chatLabelColor');

			for (i = 0; i < hide.length; i++) {
				$('#edit-window-' + hide[i]).parent().hide();
			}
		}

		$("#msnlive_color_scheme_form").toggle();
	}
	
	if ($("input[@name='block_style']:checked").val() == "1" && $("input[@name='block']:checked").val() != "1") {
		$("#msnlive_color_scheme_form").toggle();
	}
});