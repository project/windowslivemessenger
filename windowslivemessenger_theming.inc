<?php
  
  /*
   * @file
   * Theming functions for the Windows Live Messenger Module
   */

  /*
   * Theme function for the profile list table header
   */
  function theme_profilelist_header() {
    $output = "";
    $output .= "<table>";
      $output .= "<tr>";
        $output .= "<th>". t('Profile name') ."</th>";
        $output .= "<th>&nbsp;</th>";
        $output .= "<th>&nbsp;</th>";
      $output .= "</tr>";
      
    return $output;
  }
  
  /*
   * Theme function for the profile list table footer
   */
  function theme_profilelist_footer() {
    $output = "";
    $output .= "</table>";
    
    return $output;
  }
  
  /*
   * Theme function for a profile line
   */
  function theme_profilelist_profile($vars) {
    $output = "";
    
    $output .= "<tr>";
      $output .= "<td>". $vars["name"] ."</td>";
      $output .= "<td>" . l(t('Edit'), 'admin/settings/windowslivemessenger/editprofile/'. $vars['id']) ."</td>";
      $output .= "<td>". l(t('Delete'), 'admin/settings/windowslivemessenger/deleteprofile/'. $vars['id']) ."</td>";
    $output .= "</tr>";
    
    return $output;
  }
  
  /*
   * Theme function for the admin block color form
   */
  function theme_windowslivemessenger_color_form($form) {
    // Add Farbtastic color picker.
    drupal_add_css('misc/farbtastic/farbtastic.css', 'module', 'all', FALSE);
    drupal_add_css(drupal_get_path('module', 'windowslivemessenger') .'/windowslivemessenger-admin.css', 'module', 'all', FALSE);    
    drupal_add_js(drupal_get_path('module', 'windowslivemessenger') .'/js/windowslivemessenger_color.js');
    drupal_add_js('misc/farbtastic/farbtastic.js');
    
    // Add custom CSS/JS.
    $default_colors = array();

    foreach (element_children($form) as $key) {
      $default_colors[$key] = $form[$key]['#value'];
    }
    
    drupal_add_js(array('msnlive' => array('reference' => $default_colors, 'transparent' => t('none'))), 'setting');
    
    // Wrapper.
    $output .= '<div class="color-form clear-block">';
    
    // Palette.
    $output .= '<div id="msnlive-palette" class="clear-block">';
    
    foreach (element_children($form) as $key => $name) {
      $output .= drupal_render($form[$name]);
    }
    $output .= '</div>';
    
    // Render the form.
    $output .= drupal_render($form);
    // Close wrapper.
    $output .= '</div>';

    return $output;
  }